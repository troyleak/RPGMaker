RPGMaker
=========

Simple Pathfinder character sheet and NPC generator.

Allows the user to specify certain stats, and fills in the remaining stats with
random values. At the end, allows the user to modify any value to their liking

# Getting started

To begin, clone the repo and navigate to the project directory in your shell

I use python3.5's built in virtual environment creator. Your process may vary, but you do want to create a virtual environment for this project.

```
python -m venv rpgmaker
```
navigate into your venv and do

```
source ./bin/activate

python project/RPGMaker
```


# Documentation

## Character Generation

### Order
Character modules are created in this order, for ease of calculating scores.
1. Ability Scores
2. Race
3. Class
4. Skills/Feats
5. Gear
6. Attributes


### Character
Base class for items related to character creation. Each aspect of the character will be represented in this class. This object will be passed around and modified to create the final product


### Dice
Contains functions for dice rolls and modifiers. Most of the hard math will be done here


### Ability Scores
SetScores: Takes 6 values. If none are received (or they're all zero), generates random values for the character by calling dice roll_dice(sides, dice) and applies them, then calls SetMods to fill in modifiers.


### SetMods
Sets ability score modifiers based on existing stats. Called at the end of SetScores.


### Attributes
LOW PRIORITY - Right now just takes text from user and adds it to the charsheet. Eventually want to have selection from list based on race, class, etc. Will be done after other scores have been input.


### Class
Contains logic for setting the character's class.

Each of the seven classes contains a function "make" which takes the character object as an argument, and the desired level.


### Feats
TODO - Contains a list of feats and their descriptions. Contains functions to add to misc modifiers for permanent feats, and add a text description to situational feats



### Gear
AddItem(): add an item of gear to the character's backpack.
RemoveItem(): remove an item of gear from the character's backpack.
ListItems(): returns an iterable containing a list of the items in the character's backpack



### Race
Contains logic for creating a specific race character. Will be run once on creation, modifying this stat will require a regeneration of the character's base class, destroying user-defined mods and such. If you want to select a different race, I recommend backing up your character, as it will be destroyed and/or randomly regenerated. Contains a "make" function which takes the character as an argument and modifies it to include race-specific traits.


### Skills
Contains logic for adding to, subtracting from, modifying, and displaying the skill list and a variable with the number of unspent skill points.

This needs to undergo an overhaul, as the project is not currently able to create pre-leveled characters.

add_skill_points(): Function to calculate skill points. Run at level up time. Takes the character's class and intelligence and adds the appropriate amount to the skill points stat. Call this function in a loop to level up multiple times.

spend_skill_points(): Function to subtract skill points. Takes the skill, and number of levels. Automatically deducts the correct number of skill points from the character.


### TODO - Spells
