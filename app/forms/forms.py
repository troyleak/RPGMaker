# from flask_wtf import Form
from wtforms.validators import NumberRange, AnyOf, Optional
from wtforms import StringField as strf, IntegerField as intf
from wtforms import SelectField
from wtforms import FieldList
from flask.ext.wtf import Form
from wtforms import FormField, SubmitField

'''
TODO: Fix CSRF tokens
'''
colors = [
    (None, 'Choose a Color'),
    ('blue', 'Blue'),
    ('red', 'Red'),
    ('green', 'Green'),
    ('purple', 'Purple'),
    ('violet', 'Violet'),
    ('silver', 'Silver'),
    ('orange', 'Orange'),
    ('yellow', 'Yellow'),
    ('indigo', 'Indigo'),
    ('gold', 'Gold'),
    ('magenta', 'Magenta'),
    ('pink', 'Pink'),
    ('cyan', 'Cyan'),
    ('black', 'Black'),
    ('white', 'White')
]


class WTF_Attributes(Form):
    name = strf(u'Character Name')

    level = intf(u'Level', [NumberRange(0, 1), Optional()],
                 description='Lvl 1 only right now. More coming soon',
                 default=1)

    player = strf(u'Player (optional)', [Optional()])

    gender = SelectField(u'Gender', [Optional()], default='male', choices=[
        (None, 'Choose a Gender'),
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ])

    race = SelectField(u'Race', [Optional()], default='human', choices=[
        (None, 'Choose a Race'),
        ('dwarf', 'Dwarf'),
        ('elf', 'Elf'),
        ('gnome', 'Gnome'),
        ('half_elf', 'Half Elf'),
        ('half_orc', 'Half Orc'),
        ('halfling', 'Halfling'),
        ('human', 'Human')
    ])

    size = SelectField(u'Size', [Optional()], default='med', choices=[
        (None, 'Choose a Size'),
        ('xs', 'Extra Small'),
        ('sm', 'Small'),
        ('med', 'Medium'),
        ('lg', 'Large'),
        ('xl', 'Extra Large')
    ])

    age = intf(u'Age', [NumberRange(1, 9999), Optional()], default=1)
    height = intf(u'Height (cm)', [NumberRange(0, 9999), Optional()])
    weight = intf(u'Weight (kg)', [NumberRange(0, 9999), Optional()])

    hair = SelectField(u'Hair', [Optional()], choices=colors, default='white')
    eyes = SelectField(u'Eyes', [Optional()], choices=colors, default='blue')

    alignment = SelectField(u'Alignment', [Optional()],
                            default='neutral_neutral',
                            choices=[
        (None, 'Choose an Alignment'),
        ('chaotic_evil', 'Chaotic Evil'),
        ('chaotic_neutral', 'Chaotic Neutral'),
        ('chaotic_good', 'Chaotic Good'),
        ('neutral_evil', 'Neutral Evil'),
        ('neutral_neutral', 'True Neutral'),
        ('neutral_good', 'Neutral Good'),
        ('lawful_evil', 'Lawful Evil'),
        ('lawful_neutral', 'Lawful Neutral'),
        ('lawful_good', 'Lawful Good')
        ])

    # due to the sheer number and variety of deities in
    # the Pathfinder universe, I'm opting to let the user choose their
    # preference here.
    deity = strf(u'Deity', [Optional()], default="None")

    homeland = strf(u'Homeland', [Optional()], default="None")

    char_class = SelectField(u'Class', [Optional()], default='bard', choices=[
        (None, 'Choose a Class'),
        ('barbarian', 'Barbarian'),
        ('bard', 'Bard'),
        ('cleric', 'Cleric'),
        ('druid', 'Druid'),
        ('fighter', 'Fighter'),
        ('monk', 'Monk'),
        ('paladin', 'Paladin'),
        ('ranger', 'Ranger'),
        ('rogue', 'Rogue'),
        ('sorcerer', 'Sorcerer'),
        ('wizard', 'Wizard')
    ])


class WTF_Abilities(Form):

    strength = intf(u'Strength', [NumberRange(0, 21), Optional()])
    dexterity = intf(u'Dexterity', [NumberRange(0, 21), Optional()])
    constitution = intf(u'Constitution', [NumberRange(0, 21), Optional()])
    intelligence = intf(u'Intelligence', [NumberRange(0, 21), Optional()])
    wisdom = intf(u'Wisdom', [NumberRange(0, 21), Optional()])
    charisma = intf(u'Charisma', [NumberRange(0, 21), Optional()])


class WTF_Skills(Form):
    acrobatics = intf(u'Acrobatics', [NumberRange(0, 0), Optional()])
    appraise = intf(u'Appraise', [NumberRange(0, 0), Optional()])
    bluff = intf(u'Bluff', [NumberRange(0, 0), Optional()])
    climb = intf(u'Climb', [NumberRange(0, 0), Optional()])
    craft_a = intf(u'Craft a', [NumberRange(0, 0), Optional()])
    craft_b = intf(u'Craft b', [NumberRange(0, 0), Optional()])
    craft_c = intf(u'Craft c', [NumberRange(0, 0), Optional()])
    diplomacy = intf(u'Diplomacy', [NumberRange(0, 0), Optional()])
    disable_device = intf(u'Disable Device', [NumberRange(0, 0), Optional()])
    disguise = intf(u'Disguise', [NumberRange(0, 0), Optional()])
    escape_artist = intf(u'Escape Artist', [NumberRange(0, 0), Optional()])
    fly = intf(u'Fly', [NumberRange(0, 0), Optional()])
    handle_animal = intf(u'Handle Animal', [NumberRange(0, 0), Optional()])
    heal = intf(u'Heal', [NumberRange(0, 0), Optional()])
    intimidate = intf(u'Intimidate', [NumberRange(0, 0), Optional()])

    knowledge_arcana = intf(
            u'Knowledge Arcana',
            [NumberRange(0, 0), Optional()])
    knowledge_dungeoneering = intf(
            u'Knowledge - Dungeoneering',
            [NumberRange(0, 0), Optional()])
    knowledge_engineering = intf(
            u'Knowledge - Engineering',
            [NumberRange(0, 0), Optional()])
    knowledge_geography = intf(
            u'Knowledge - Geography',
            [NumberRange(0, 0), Optional()])
    knowledge_history = intf(
            u'Knowledge - History',
            [NumberRange(0, 0), Optional()])
    knowledge_local = intf(
            u'Knowledge - Local',
            [NumberRange(0, 0), Optional()])
    knowledge_nature = intf(
            u'Knowledge - Nature',
            [NumberRange(0, 0), Optional()])
    knowledge_nobility = intf(
            u'Knowledge - Nobility',
            [NumberRange(0, 0), Optional()])
    knowledge_planes = intf(
            u'Knowledge - Planes',
            [NumberRange(0, 0), Optional()])
    knowledge_religion = intf(
            u'Knowledge - Religion',
            [NumberRange(0, 0), Optional()])

    linguistics = intf(u'Linguistics', [NumberRange(0, 0), Optional()])
    perception = intf(u'Perception', [NumberRange(0, 0), Optional()])
    perform_a = intf(u'Perform A', [NumberRange(0, 0), Optional()])
    perform_b = intf(u'Perform B', [NumberRange(0, 0), Optional()])
    profession_a = intf(u'Profession A', [NumberRange(0, 0), Optional()])
    profession_b = intf(u'Profession B', [NumberRange(0, 0), Optional()])
    ride = intf(u'Ride', [NumberRange(0, 0), Optional()])
    sense_motive = intf(u'Sense Motive', [NumberRange(0, 0), Optional()])
    sleight_of_hand = intf(u'Sleight of Hand', [NumberRange(0, 0), Optional()])
    spellcraft = intf(u'Spellcraft', [NumberRange(0, 0), Optional()])
    stealth = intf(u'Stealth', [NumberRange(0, 0), Optional()])
    survival = intf(u'Survival', [NumberRange(0, 0), Optional()])
    swim = intf(u'Swim', [NumberRange(0, 0), Optional()])
    use_magic_device = intf(
                u'Use Magic Device', [NumberRange(0, 0), Optional()])


class WTF_Weapon(Form):

    name = strf(u'Name', [Optional()])
    attack_bonus = intf(u'Attack Bonus', [NumberRange(0, 0), Optional()])
    critical = intf(u'Critical', [NumberRange(0, 0), Optional()])
    dmg_type = intf(u'Damage Type', [NumberRange(0, 0), Optional()])
    wpn_range = intf(u'Weapon Range', [NumberRange(0, 0), Optional()])
    ammunition = intf(u'Ammunition', [NumberRange(0, 0), Optional()])
    damage = strf(u'Damage', [Optional()])


class WTF_Armor(Form):

    name = strf(u'Name', [Optional()])
    bonus = intf(u'Bonus', [NumberRange(0, 0), Optional()])
    armor_type = strf(u'Armor Type', [Optional()])
    check_penalty = intf(u'Check Penalty', [NumberRange(0, 0), Optional()])
    spell_failure = intf(u'Spell Failure', [NumberRange(0, 0), Optional()])
    weight = intf(u'Weight', [NumberRange(0, 0), Optional()])
    properties = strf(u'Properties', [Optional()])


class WTF_Item(Form):

    item = strf(u'Item', [Optional()])
    weight = intf(u'Weight', [NumberRange(0, 0), Optional()])


class WTF_Charsheet(Form):
    attributes = FormField(WTF_Attributes)
    abilities = FormField(WTF_Abilities)
    skills = FormField(WTF_Skills)
    weapon = FieldList(FormField(WTF_Weapon), min_entries=1)
    armor = FieldList(FormField(WTF_Armor), min_entries=1)
    item = FieldList(FormField(WTF_Item), min_entries=1)
