# from flask_wtf import Form
from wtforms.validators import *
from wtforms import Form
from wtforms import FormField, SubmitField, StringField, IntegerField
from wtforms import validators

# TODO: Debug validate input - Required input works, but numberrange doesn't
#       Plug frontend into backend


class WTF_Attributes(Form):
    name = StringField(u'Name')
    level = IntegerField(u'Level')


class WTF_Abilities(Form):
    strength = IntegerField(u'Strength')


class WTF_Skills(Form):
    acrobatics = IntegerField(u'Acrobatics')


class WTF_Weapon(Form):
    name = StringField(u'Name')


class WTF_Armor(Form):
    name = StringField(u'Name')


class WTF_Item(Form):
    item = StringField(u'Item')


class WTF_Charsheet(Form):
    attributes = FormField(WTF_Attributes)
    abilities = FormField(WTF_Abilities)
    skills = FormField(WTF_Skills)
    weapon = FormField(WTF_Weapon)
    armor = FormField(WTF_Armor)
    item = FormField(WTF_Item)
    submit = SubmitField('Submit')
