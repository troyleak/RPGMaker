#!/bin/bash

# Install dependencies
sudo apt-get update
sudo apt-get install -y python3 python3-pip
sudo pip3 install flask flask_wtf flask_bootstrap wtforms_json

# Replace /etc/rc.local with our customized one
sudo cp /etc/rc.local /etc/rc.local.old
cat /vagrant/autostart.sh > /etc/rc.local
