
'''
Saving throws and their associated functions
'''


class SavingThrows():

    def __init__(self):

        self.saving_throws = {
            'fortitude': {
                'score': 0,
                'mod': 0,
                'magic_mod': 0,
                'misc_mod': 0
                },
            'reflex': {
                'score': 0,
                'mod': 0,
                'magic_mod': 0,
                'misc_mod': 0
                },
            'will': {
                'score': 0,
                'mod': 0,
                'magic_mod': 0,
                'misc_mod': 0
                }
            }

    def set_saving_throws(self, **kwargs):
        # takes a name and dict of values to modify
        if 'fortitude' in kwargs.items():
            self.saving_throws['fortitude'] = kwargs['fortitude']
        elif 'will' in kwargs.items():
            self.saving_throws['will'] = kwargs['will']
        elif 'reflex' in kwargs.items():
            self.saving_throws['reflex'] = kwargs['reflex']
        else:
            print("No valid saving throws to modify")

    def get_saving_throws(self, *args):
        for i in args:
            try:
                print(self.saving_throws[i])
            except KeyError:
                print("Error finding value {0}".format(i))
