
import json
from . import ability_scores, saving_throws, attributes, classes
from . import feats, gear, race, skills, spells

'''
This is the main class for the model of the character

It contains the submitted values for a character after form submission.

Of note are the functions to assign character values, and to return the
    character object as JSON formatted data

Before form submission, the object contains empty values. The subclasses are
    also configured likewise (for ones in which that is possible).

After form submission, blank values are randomized (within the bounds of
    the other specified values). This means the system is 'lazy' in its
    creating your character.

After the character has been created, it is not possible to go back and alter
    it. I recommend starting from scratch. It may be helpful to outline some
    basic characteristics on paper before using this tool, as it is very much
    in alpha stages and could explode at any moment.

Because of the complexity involved in leveling up a character, the opposite is
    low on my priorities compared to implementing things like feats and spells

'''


class Character():
    # TODO: CHARACTER - Add feats, spells, gear, money, and experience to character

    def __init__(self):

        self.modules = {
            'abilities': ability_scores.Abilities(),
            'saving_throws': saving_throws.SavingThrows(),
            'race': race.Race(),
            'char_class': classes.Char_Class(),
            'skills': skills.Skills(),
            'feats': feats.Feats(),
            'spells': spells.Spells(),
            'gear': gear.Gear(),
            'attributes': attributes.Attributes(),
            'money': 0,
            'experience': 0
        }

    def update_abilities(self, module):
        pass

    def update_saving_throws(self, fort_scores, will_scores, ref_scores):
        new_saving_throws = self.saving_throws.SavingThrows()

        fort = {
            'score': fort_scores['score'],
            'mod': fort_scores['mod'],
            'magic_mod': fort_scores['magic_mod'],
            'misc_mod': fort_scores['misc_mod']
        },

        ref = {
            'score': ref_scores['score'],
            'mod': ref_scores['mod'],
            'magic_mod': ref_scores['magic_mod'],
            'misc_mod': ref_scores['misc_mod']
        },

        will = {
            'score': will_scores['score'],
            'mod': will_scores['mod'],
            'magic_mod': will_scores['magic_mod'],
            'misc_mod': will_scores['misc_mod']
        }

        new_saving_throws.set_saving_throws(fortitude=fort, reflex=ref, will=will)

        self.modules['saving_throws'] = new_saving_throws

    def update_race(self, module):
        # TODO: CHARACTER - Create function to update character race
        pass

    def update_char_class(self, module):
        # TODO: CHARACTER - Create function to update char_class
        pass

    def update_skills(self, module):
        # TODO: CHARACTER - Create function to update skills
        pass

    def update_feats(self, module):
        # TODO: CHARACTER - Create function to update feats
        pass

    def update_spells(self, module):
        # TODO: CHARACTER - Create function to update spells
        pass

    def update_gear(self, module):
        # TODO: CHARACTER - Create function to update gear
        pass

    def update_attributes(self, module):
        # TODO: CHARACTER - Create function to update attributes
        pass

    def update_money(self, module):
        # TODO: CHARACTER - Create function to update money
        pass

    def update_experience(self, module):
        # TODO: CHARACTER - Create function to update experience
        pass
