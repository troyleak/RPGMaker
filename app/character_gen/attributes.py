import random

'''
Contains data structures pertaining to character's physical attributes.

Also contains modifiers for base stats. This class will be used last after
race, class, skills, feats, etc. have been selected

self.options: contains a dict with the available options for each
    physical attribute

set_attrib takes an attribute and a value and modifies the character
    appropriately, if the desired attribute and option exist.

TODO: ATTRIBUTES - Create function to modify misc_modifiers
'''


class Attributes():
    def __init__(self):
        self.options = {
            'gender': ['male', 'female'],
            'size': ['large', 'medium', 'small'],
            'age': list(range(18, 400)),
            'height': list(range(100, 200)),
            'weight': list(range(150, 300)),
            'hair': ['red', 'orange', 'yellow', 'green', 'blue',
                     'indigo', 'violet', 'black', 'white'],
            'eyes': ['red', 'orange', 'yellow', 'green', 'blue',
                     'indigo', 'violet', 'black', 'white'],
            'alignment': ['chaotic_evil', 'chaotic_neutral', 'chaotic_good',
                          'neutral_evil', 'neutral_neutral', 'neutral_good',
                          'lawful_evil', 'lawful_neutral', 'lawful_good']
        }

        self.attributes = {
            'gender': 'none',
            'size': 'none',
            'age': 0,
            'height': 0,
            'weight': 0,
            'hair': 'none',
            'eyes': 'none',
            'alignment': 'none'
        }

        # misc modifiers
        self.misc_modifiers = {
            'base attack bonus': 0,
            'spell resist': 0,
            'armor class': 10,
            'armor class (touch)': 0,
            'armor class (flat)': 0,
            'speed': 0,
            'initiative': 0,
            'hit points': 0,
            'armor bonus': 0,
            'shield bonus': 0,
            'dodge bonus': 0,
            'size modifier': 0,
            'natural armor': 0,
            'deflection modifier': 0,
            'misc. armor mod': 0,
            'combat maneuver bonus': 0,
            'combat maneuver defense': 10,
            'number of feats': 0,
            'languages': [],
            'spellcaster': False,
            'spell failure penalty': 0,
            'level': 1,
            'favored HP': True,
            'build': 'Random'
        }

    def set_attrib(self, attrib, value):
        # takes an attribute and a value and sets the attrib to the passed val
        try:
            self.attributes[attrib] = value
        except KeyError:
            print("Error modifying that attribute. Perhaps it doesn't exist?")

    def set_attribs_rand(self):
        # sets attributes randomly
        for i in self.attributes:
            self.attributes[i] = random.choice(self.options[i])
