class Skills():
    def __init__(self):
        self.skills = {
            'acrobatics': 0,
            'appraise': 0,
            'bluff': 0,
            'climb': 0,
            'craft a': 0,
            'craft b': 0,
            'craft c': 0,
            'diplomacy': 0,
            'disable device': 0,
            'disguise': 0,
            'escape artist': 0,
            'fly': 0,
            'handle animal': 0,
            'heal': 0,
            'intimidate': 0,
            'knowledge arcana': 0,
            'knowledge dungeoneering': 0,
            'knowledge engineering': 0,
            'knowledge geography': 0,
            'knowledge history': 0,
            'knowledge local': 0,
            'knowledge nature': 0,
            'knowledge nobility': 0,
            'knowledge planes': 0,
            'knowledge religion': 0,
            'linguistics': 0,
            'perception': 0,
            'perform a': 0,
            'perform b': 0,
            'profession a': 0,
            'profession b': 0,
            'ride': 0,
            'sense motive': 0,
            'sleight of hand': 0,
            'spellcraft': 0,
            'stealth': 0,
            'survival': 0,
            'swim': 0,
            'use magic device': 0}

    def set_skill(self, skill, points):
        # Takes the skill and the value as arguments.
        # Updates the value, not adding to it
        # If it can't find the skill displays a warning message
        if skill in self.skills:
            self.skills[skill] = points
            print("Updated skill" + skill + " with the value")
        else:
            print("Error updating skill value")
        print("test skills.Skills.update()")

    def get_skill(self, skill):
        if skill in self.skills:
            return skill
        else:
            print("Unable to update " + skill + " skill")
