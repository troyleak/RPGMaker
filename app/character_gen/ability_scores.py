'''
Abilities

'''


from . import dice


class Abilities():

    def __init__(self):

        self.stats = {
            'strength': {
                'score': 0,
                'mod': 0
            },
            'dexterity': {
                'score': 0,
                'mod': 0
            },
            'constitution': {
                'score': 0,
                'mod': 0
            },
            'intelligence': {
                'score': 0,
                'mod': 0
            },
            'wisdom': {
                'score': 0,
                'mod': 0
            },
            'charisma': {
                'score': 0,
                'mod': 0
            }
        }

    def set_ability_score(self, stat, value):
        try:
            self.stats[stat][score] = value
            self.stats[stat][mod] = set_ability_mod(value)
        except KeyError:
            print("Cannot modify stat {0}".format(stat))

    def set_ability_scores(self, **kwargs):
        for i in self.stats.keys():
            if i in kwargs:
                self.stats[i] = kwargs[i]

    def set_ability_stat_rand(self, stat):
        score = sum(dice.drop_lowest(i for i in dice.roll_dice(6, 4)))
        self.stats[stat] = {
            score,
            get_ability_mod_from_score(score)
        }

    def get_ability_mod_from_score(self, stat):
        # returns the ability modifier for a given stat
        if stat in [None, '', "", 0]:
            return 0

        elif stat >= 10:
            stat = (stat - 10) / 2
        elif stat < 10:
            stat = (11 - stat) / 2 * -1

        return int(stat)

    def get_ability_scores(self):
        for i in self.stats:
            yield i['score']

    def get_ability_score(self, stat):
        try:
            return self.stats[stat][score]
        except:
            print("Not able to find that stat")
            return None

    def get_ability_mods(self):
        for i in self.stats:
            yield i['mod']
