__all__ = [
    "test_ability_scores", "test_attributes", "test_character",
    "test_classes", "test_dice", "test_feats", "test_gear", "test_race",
    "test_skills"]
